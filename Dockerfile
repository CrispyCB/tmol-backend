FROM node:16

WORKDIR /usr/app

COPY package*.json ./

RUN npm install --silent

RUN npm install knex -g

COPY server/ ./server

COPY db/ ./db

CMD ["npm", "run", "start"]

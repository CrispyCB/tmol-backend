/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async (knex) => {
    await knex.schema.hasTable('todos').then((exists) => {
      if (!exists) {
        return knex.schema.createTable('todos', (table) => {
          table.increments('id');
          table.string('todo').notNullable();
          table.boolean('done').defaultTo(false);
          table.timestamps(true, true, false);
          table.string('created_by').notNullable();
        });
      }
    });
  };
  
  /**
   * @param { import("knex").Knex } knex
   * @returns { Promise<void> }
   */
  exports.down = async (knex) => knex.schema.dropTableIfExists('todos');
  
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('todos').del()
    .then(function () {
      // Inserts seed entries
      return knex('todos').insert([
        { todo: 'Deliver pizza.', createdby: 'nick.davis'},
        { todo: 'Rob bank.', createdby: 'nick.davis'},
        { todo: 'Stop Dwayne + Travis.', createdby: 'nick.davis'}
      ]);
    });
};

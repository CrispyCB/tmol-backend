CREATE TABLE IF NOT EXISTS todos (
    id serial PRIMARY KEY,
    todo varchar(256) NOT NULL,
    done boolean DEFAULT false,
    created_at timestamp DEFAULT now(),
    updated_at timestamp DEFAULT now(),
    created_by varchar(256) NOT NULL
);


const express = require('express');
const router = express.Router();
const dotenv = require("dotenv").config();
const passport = require("passport");
const GoogleStrategy = require('passport-google-oauth20');
const session = require("express-session");
const crypto = require('crypto');

//build session middleware
router.use(
    session({
        genid: function(req) {
            return crypto.randomUUID(); // use UUIDs for session IDs
          },
        secret: "this is a secret",
        resave: true,
        saveUninitialized: true
    })
  );
  //initialize passport and set up sessions so that users can be logged to specific sessions
  router.use(passport.initialize());
  router.use(passport.session());

  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        callbackURL: '/auth/google/callback',
        scope: ["profile", "email"]
      },
      function(accessToken, refreshToken, profile, done) {
        const user = profile;
        console.log(user);
        return done(null, user);
      }
    )
  );
  //here you serialize a user - that is, create a session that is pegged to that specific user.
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  //and here you deserialize that user - removing/destroying the session you created earlier.
  passport.deserializeUser((id, done) => {
    const user = id;
    done(user);
  });


//middleware function to check whether user is authenticated.
//if the user is authenticated, let them continue on with what they were doing.
//if user is *not* authenticated, boot them back to the homepage to sign in.
function checkLoginStatus(req, res, next) {
    if (req.isAuthenticated()) {
      next();
    } else {
      res.redirect("/");
    }
  }
  
  //initial request to Google for authentication, using the GoogleStrategy defined above.
  //the authentication request is scoped to profile && email, so only that information will be revealed to passport.
  //Depending on which API scope you use, different information will be revealed to passport as part of the auth process.
  router.get(
    "/auth/google",
    passport.authenticate("google")
  );
  //the callback endpoint that we used as part of the URL above.
  //if the authentication succeeds, we get redirected to /success. If it fails, we get redirected to /failure.
  router.get(
    "/auth/google/callback",
    passport.authenticate("google", {
      failureRedirect: "/failure"
    }),
    function(req, res) {
      res.redirect("/success");
    }
  );
  //actual success route.
  //checkLoginStatus is middleware function we defined above
  router.get("/success", checkLoginStatus, (req, res) => {
    res.send(req.user);
  });
  //actual failure route.
  //checkLoginStatus is middleware function we defined above.
  //Note that we log the user here - this way we can see *why* the request failed.
  router.get("/failure", checkLoginStatus, (req, res) => {
    console.log(req.user);
    res.send("Authentication failed.");
  });

  module.exports = router;
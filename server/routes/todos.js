const express = require('express');
const router = express.Router();
const db = require('../../db/db');

router.post("/", async (req, res) => {
    const createdTodo = await db('todos').insert({ 'todo': req.body.todo, 'created_by': req.body.createdBy }, ['id', 'todo', 'done', 'created_at', 'updated_at', 'created_by']);
    res.status(201).send(createdTodo[0]);
})

router.get("/", async (req, res) => {
    const todos = await db('todos').select('*').whereRaw("created_at > NOW() - INTERVAL '30 minutes'")
    res.send(todos);
})

router.patch("/complete/:id", async (req, res) => {
    await db('todos').update({ 'done': true }).where('id', req.params.id);
    res.sendStatus(200).end();
})

router.delete("/", async (req, res) => {
    await db('todos').del();
    res.sendStatus(200).end();
})

router.delete("/:id", async (req, res) => {
    await db('todos').where('id', req.params.id).del();
    res.sendStatus(200).end();
})

module.exports = router;
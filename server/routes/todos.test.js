describe('Todo tests', () => {
    const supertest = require('supertest');
    const server = require('./../server');
    const request = supertest(server);
    it('should be able to GET all todos', async () => {
        const actualTodos = [{'todo': 'Deliver pizza.'}, {'todo': 'Rob bank.'}, {'todo': 'Stop Dwayne + Travis.'}];
        const getAllTodosResponse = await request.get('/todos');
        const expectedTodos = getAllTodosResponse.body.map(todo => todo.todo);
        for (i = 0; i < actualTodos.length; i++) {
            expect(actualTodos[i].todo).toBe(expectedTodos[i]);
        };
    });
    it('should be able to POST a new todo', async () => {
        const createNewTodosResponse = await request.post('/todos').send({'todo': "Finish writing Jest tests.", 'createdBy': "nick.davis"});
        expect(createNewTodosResponse.statusCode).toBe(201);
        expect(createNewTodosResponse.body.todo).toBe("Finish writing Jest tests.");
    })
    it('should be able to PATCH a todo', async () => {
        const completeTodosResponse = await request.patch('/todos/complete/1');
        expect(completeTodosResponse.statusCode).toBe(200);
    });
    it('should be able to DELETE a single todo', async () => {
        const deleteSingleTodoResponse = await request.delete('/todos/1');
        expect(deleteSingleTodoResponse.statusCode).toBe(200);
    });
    it('should be able to DELETE all todos', async () => {
        const deleteAllTodoResponse = await request.delete('/todos');
        expect(deleteAllTodoResponse.statusCode).toBe(200);
    });
});
const express = require('express');
const cors = require('cors');

const server = express();
server.use(express.urlencoded({extended: true})); 
server.use(express.json());
server.use(cors())

const todos = require('./routes/todos');
const auth = require('./routes/auth');
server.use('/todos', todos);
server.use('/', auth);

module.exports = server;